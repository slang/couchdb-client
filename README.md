# CouchDB Client

[![Build Status](http://img.shields.io/travis/slang800/couchdb-client.svg?style=flat-square)](https://travis-ci.org/slang800/couchdb-client) [![NPM version](http://img.shields.io/npm/v/couchdb-client.svg?style=flat-square)](https://www.npmjs.org/package/couchdb-client) [![NPM license](http://img.shields.io/npm/l/couchdb-client.svg?style=flat-square)](https://www.npmjs.org/package/couchdb-client)

## Install

CouchDB Client is an [npm](http://npmjs.org/package/couchdb-client) package, so it can be installed into your project like this:

```bash
npm install couchdb-client --save
```

## Usage

If you want pretty printing, pipe the output into [jq](https://stedolan.github.io/jq/manual/).

## Similar Tools

- [couchdb-utils](https://github.com/awilliams/couchdb-utils)
- [futon (cli)](https://www.npmjs.com/package/futon)
